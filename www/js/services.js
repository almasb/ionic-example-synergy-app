angular.module('app.services', [])

  .service('userService', function () {
    var newUser = false;
    var setNewUser = function (value) {
      newUser = value;
    };
    var getNewUser = function () {
      return newUser;
    };
    return {
      setNewUser : setNewUser,
      getNewUser : getNewUser
    };
  })
  .service('editUser', function () {
    var editUser = false;
    var setEditUser = function (value) {
      editUser = value;
    };
    var getEditUser = function () {
      return editUser;
    };
    return {
      setEditUser : setEditUser,
      getEditUser : getEditUser
    };
  })
  .service('convertImage', function () {
    var getDataUri = function (url, callback) {
      var image = new Image();
      image.onload = function () {
        var canvas = document.createElement('canvas');
        canvas.width = this.naturalWidth; // or 'width' if you want a special/scaled size
        canvas.height = this.naturalHeight; // or 'height' if you want a special/scaled size

        canvas.getContext('2d').drawImage(this, 0, 0);

        // Get raw image data
        callback(canvas.toDataURL('image/png').replace(/^data:image\/(png|jpg);base64,/, ''));

        // ... or get as Data URI
        //callback(canvas.toDataURL('image/png'));
      };
      image.src = url;
    }
    return {
      getDataUri: getDataUri
    }
  })
  .factory('ParseEmployee', ['Parse', function (Parse) {
    var ParseEmployee = Parse.Object.extend('Employees');
    Parse.defineAttributes(ParseEmployee, ['fullName', 'picture', 'title', 'location'])
    return ParseEmployee;
  }]);

