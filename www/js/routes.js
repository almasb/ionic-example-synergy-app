angular.module('app.routes', [])

.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider) {
  $ionicConfigProvider.navBar.alignTitle('center');
  $stateProvider

    .state('login', {
      url: '/login',
      templateUrl: 'templates/login.html',
      controller: 'loginCtrl'
    })
    .state('signup', {
      url: '/signup',
      templateUrl: 'templates/signup.html',
      controller: 'signupCtrl'
    })
    .state('synergyApp.directory', {
      url: '/directory',
      cache: false,
      views: {
        'menuContent': {
          templateUrl: 'templates/directory.html',
          controller: 'directoryCtrl'
        }
      }
    })
    .state('synergyApp.profile', {
      url: '/profile',
      views: {
        'menuContent': {
          templateUrl: 'templates/profile.html',
          controller: 'employeeCtrl'
        }
      }
    })
    .state('synergyApp.editProfile', {
      url: '/edit-profile',
      views: {
        'menuContent': {
          templateUrl: 'templates/editProfile.html',
          controller: 'editProfileCtrl'
        }
      }
    })
    .state('synergyApp.add', {
      url: '/add-employee',
      views: {
        'menuContent': {
          templateUrl: 'templates/addEmployee.html',
          controller: 'addEmployeeCtrl'
        }
      }
    })
    .state('synergyApp.edit', {
      url: '/edit-employee',
      views: {
        'menuContent': {
          templateUrl: 'templates/editEmployee.html',
          controller: 'employeeCtrl'
        }
      }
    })
    .state('synergyApp', {
      url: '/app',
      abstract:true,
      templateUrl: 'templates/menu.html',
      onEnter: function($state, Parse){
        if(!Parse.User.current()){
          $state.go('login');
        }
      }
    });
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/directory');

});
