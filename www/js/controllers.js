angular.module('app.controllers', [])

  .controller('loginCtrl', function($scope, $state, Parse, userService) {
    $scope.loginData = {};
    $scope.warning = false;
    $scope.newUser = false;
    if(userService.getNewUser()) {
      $scope.newUser = userService.getNewUser();
    }
    Parse.User.logOut();
    $scope.doLogin = function (e) {
      Parse.User.logIn($scope.loginData.username, $scope.loginData.password, {
        success: function(user) {
          $state.go("synergyApp.directory");
        },
        error: function (user, error) {
          // Show the error message somewhere and let the user try again.
          $scope.newUser = false;
          $scope.warning = error.message;
        }
      });
    }
  })
  .controller('signupCtrl', function($scope, $state, Parse, ParseEmployee, userService) {
    $scope.signUpForm = {};
    $scope.warning = null;
    userService.setNewUser(false);

    $scope.signUp = function (fullName, username, email, password) {
      var query = new Parse.Query(ParseEmployee);
      query.equalTo('fullName', fullName);
      query.find().then(function (employees) {
        if(employees.length === 0) {
          $scope.warning = 'No Synergy employee found. Please ask your manager to add you.';
        } else {
          var user = new Parse.User();
          user.set('isAdmin', false);
          user.set('employee', employees[0]);
          user.set("username", username);
          user.set("password", password);
          user.set("email", email);

          user.signUp(null, {
            success: function (user) {
              fullName, username, email, password = null;
              userService.setNewUser(username);
              $state.go('login');
            },
            error: function (user, error) {
              // Show the error message somewhere and let the user try again.
              $scope.warning = error.message;
            }
          });
        }
      });
    }
  })
  .controller('directoryCtrl', function($scope, $state, Parse, ParseEmployee, editUser) {
    $scope.shouldShowDelete = false;
    $scope.listCanSwipe = true;
    $scope.items = [];
    $scope.getEmployees = function () {
      new Parse.Query(ParseEmployee).find().then(function (employees) {
        $scope.items = employees
      });
    }
    $scope.getEmployees();
    $scope.isAdmin = function () {
      return Parse.User.current().get('admin');
    }
    $scope.showProfile = function(item) {
      editUser.setEditUser(item);
      $state.go('synergyApp.profile');
    }
    $scope.add = function () {
      $state.go('synergyApp.add');
    };
    $scope.edit = function (item) {
      editUser.setEditUser(item);
      $state.go('synergyApp.edit');
    };
    $scope.delete = function (item, index) {
      item.destroy({
        success: function (obj) {
          $scope.items.splice(index, 1);
        },
        error: function(obj, error) {
          alert(error);
        }
      })
    }
  })
  .controller('employeeCtrl', function($scope, $state, Parse, ParseEmployee, editUser) {
    $scope.user = editUser.getEditUser();
    $scope.picture = 'img/profilepic.png'
    if($scope.user.get('picture')) {
      $scope.picture = $scope.user.picture.url();
    }

    var imgString = null;

    $scope.getPhoto = function(source) {
      var options = {
        quality: 80,
        targetWidth: 800,
        targetHeight: 800,
        allowEdit : true,
        destinationType: Camera.DestinationType.DATA_URL,
        sourceType: source,
        encodingType: 0
      }
      navigator.camera.getPicture(onSuccess,onFail,options);
    }

    var onSuccess = function(imageData) {
      $scope.picture = 'data:image/jpeg;base64,' + imageData;
      imgString = imageData;
      $scope.$apply();
    };
    var onFail = function(e) {
      console.log("On fail " + e);
    }
    $scope.updateEmployee = function () {
      if(imgString) {
        var parseFile= new Parse.File("profilePic.png", {base64:imgString});
        $scope.user.set("picture", parseFile);
      }
      $scope.user.save().then(function (emp) {
        $state.go('synergyApp.directory');
      });
    }
  })
  .controller('addEmployeeCtrl', function($scope, $state, $ionicHistory, Parse, convertImage) {
    $scope.userForm = {};
    $scope.user = new Parse.Object("Employees");
    $scope.picture = 'img/profilepic.png';
    var parseFile = new Parse.File();
    var imgString = null;
    $scope.getPhoto = function(source) {
      var options = {
        quality: 80,
        targetWidth: 800,
        targetHeight: 800,
        destinationType: Camera.DestinationType.DATA_URL,
        sourceType: source,
        encodingType: 1
      }
      navigator.camera.getPicture(onSuccess,onFail,options);
    }
    var onSuccess = function(imageData) {
      $scope.picture = 'data:image/png;base64,' + imageData;
      imgString = imageData;
      $scope.$apply();
    };
    var onFail = function(e) {
      console.log("On fail " + e);
    }
    $scope.addEmployee = function (name) {
      if(imgString) {
        parseFile = new Parse.File("profilePic.png", {base64:imgString});
      } else {
        parseFile = null;
      }
      $scope.user.set("picture", parseFile);
      $scope.user.set('createdBy', Parse.User.current());
      $scope.user.set("fullName", name);
      $scope.user.save().then(function (emp) {
        $ionicHistory.nextViewOptions({
          disableBack: true
        });
        $state.go('synergyApp.directory');
      });

    }
  })
  .controller('editProfileCtrl', function($scope, $state, $ionicHistory, Parse) {
    $scope.user = Parse.User.current().get('employee');
    if($scope.user.get('picture')) {
      $scope.picture = $scope.user.get('picture').url();
    }
    var imgString = null;

    $scope.getPhoto = function(source) {
      var options = {
        quality: 80,
        targetWidth: 800,
        targetHeight: 800,
        destinationType: Camera.DestinationType.DATA_URL,
        sourceType: source,
        encodingType: 1
      }
      navigator.camera.getPicture(onSuccess,onFail,options);
    }

    var onSuccess = function(imageData) {
      $scope.picture = 'data:image/png;base64,' + imageData;
      imgString = imageData;
      $scope.$apply();
    };
    var onFail = function(e) {
      console.log("On fail " + e);
    }
    $scope.updateProfile = function () {
      if(imgString) {
        var parseFile= new Parse.File("profilePic.png", {base64:imgString});
        $scope.user.set("picture", parseFile);
      }
      $scope.user.save().then(function (emp) {
        $ionicHistory.nextViewOptions({
          disableBack: true
        });
        $state.go('synergyApp.directory');
      });
    }
  });
